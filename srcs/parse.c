/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	valid_file(char *line)
{
	int		i;

	i = 0;
	while (line[i])
	{
		if (!(line[i] == '.') && !(line[i] >= 'a' && line[i] <= 'z') &&
				!(line[i] == '	') && !(line[i] >= '0' && line[i] <= '9') &&
				!(line[i] == '-'))
			ft_error("error: One or many invalid character in file", 2);
		i++;
	}
}

void	parse_line(t_env *e, char *line)
{
	valid_file(line);
	get_info(e, line);
}

void	check_argc(int ac)
{
	if (ac < 2)
		ft_error("error: Run the program with one scene", 2);
	if (ac > 2)
		ft_error("error: Run the program with only one scene", 2);
}

void	parse_file(t_env *e, int ac, char *av)
{
	int		fd;
	int		count;
	char	*line;

	count = 0;
	check_argc(ac);
	if ((fd = open(av, O_RDONLY)) < 0)
		ft_error("error: Open failed in parse_file.c", 2);
	while ((get_next_line(fd, &line)) > 0)
	{
		if (line[0] != '#' && line[0] != '\0')
		{
			count = 1;
			parse_line(e, line);
		}
	}
	if (count == 0)
		ft_error("error: Nothing was read", 2);
}
