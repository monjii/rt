/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math_primitives.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	math_paraboloid(double *res, t_ray *ray, t_obj *ptr)
{
	t_vec	point;

	point = sub_vec(&ray->origin, &ptr->pos);
	res[0] = dot(&ray->dir, &ray->dir) - pow(dot(&ray->dir, &ptr->dir), 2.0);
	res[1] = dot(&ray->dir, &point) - (dot(&ray->dir, &ptr->dir) *
		(dot(&point, &ptr->dir) + 2.0 * ptr->slide));
	res[1] *= 2.0;
	res[2] = dot(&point, &point) - (dot(&point, &ptr->dir) *
		(dot(&point, &ptr->dir) + 4.0 * ptr->slide));
}

void	math_sphere(double *res, t_ray *ray, t_obj *ptr)
{
	res[0] = pow(ray->dir.x, 2.0) + pow(ray->dir.y, 2.0) +
		pow(ray->dir.z, 2.0);
	res[1] = 2.0 * (ray->dir.x * (ray->origin.x - ptr->pos.x) + ray->dir.y *
	(ray->origin.y - ptr->pos.y) + ray->dir.z * (ray->origin.z - ptr->pos.z));
	res[2] = (pow(ray->origin.x - ptr->pos.x, 2.0) +
		pow(ray->origin.y - ptr->pos.y, 2.0) + pow(ray->origin.z -
			ptr->pos.z, 2.0)) - pow(ptr->radius, 2.0);
}

void	math_cylinder(double *res, t_ray *ray, t_obj *ptr)
{
	t_vec	point;

	point = sub_vec(&ray->origin, &ptr->pos);
	res[0] = dot(&ray->dir, &ray->dir) - pow(dot(&ray->dir, &ptr->dir), 2.0);
	res[1] = 2.0 * (dot(&ray->dir, &point) - dot(&ray->dir, &ptr->dir) *
		dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) - pow(dot(&point, &ptr->dir), 2.0) -
		pow(ptr->radius, 2.0);
}

void	math_cone(double *res, t_ray *ray, t_obj *ptr)
{
	t_vec	point;

	point = sub_vec(&ray->origin, &ptr->pos);
	res[0] = dot(&ray->dir, &ray->dir) - (1.0 + ptr->angle * ptr->angle) *
		dot(&ray->dir, &ptr->dir) * dot(&ray->dir, &ptr->dir);
	res[1] = 2.0 * (dot(&ray->dir, &point) - (1.0 + ptr->angle * ptr->angle) *
		dot(&ray->dir, &ptr->dir) * dot(&point, &ptr->dir));
	res[2] = dot(&point, &point) - (1.0 + ptr->angle * ptr->angle) *
		dot(&point, &ptr->dir) * dot(&point, &ptr->dir);
}
