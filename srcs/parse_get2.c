/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_get2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		get_camera_pos(t_env *e, char **file, int i)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 4);
	count_error[3] = check_camera_pos(file, i, count_error);
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->camera->pos.x = ft_atof(file[i + 1]);
	}
	if (count_error[1] != 1)
	{
		if (is_pos(file[i + 2]) == 1)
			return (count_error[3] = 2);
		e->camera->pos.y = ft_atof(file[i + 2]);
	}
	if (count_error[2] != 1)
	{
		if (is_pos(file[i + 3]) == 1)
			return (count_error[3] = 3);
		e->camera->pos.z = ft_atof(file[i + 3]);
	}
	return (count_error[3]);
}

int		check_obj_pos(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 1);
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 2);
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 3);
	}
	return (count_error[3] = 3);
}

int		get_obj_pos(t_env *e, char **file, int i)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 4);
	check_obj_pos(e, file, i, count_error);
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->obj->last->pos.x = ft_atof(file[i + 1]);
	}
	if (count_error[1] != 1)
	{
		if (is_pos(file[i + 2]) == 2)
			return (count_error[3] = 2);
		e->obj->last->pos.y = ft_atof(file[i + 2]);
	}
	if (count_error[2] != 1)
	{
		if (is_pos(file[i + 3]) == 3)
			return (count_error[3] = 3);
		e->obj->last->pos.z = ft_atof(file[i + 3]);
	}
	return (count_error[3]);
}

int		get_rotate(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 4);
	check_rotate_info(file, i, count_error);
	if (count_error[0] != 1)
		return (camerax_or_not(e, file, i, type));
	if (count_error[1] != 1)
		return (cameray_or_not(e, file, i, type));
	if (count_error[2] != 1)
		return (cameraz_or_not(e, file, i, type));
	return (count_error[3]);
}

int		get_position(t_env *e, char **file, int i, int type)
{
	if (type == LIGHT)
		return (get_light_pos(e, file, i));
	else if (type == CAMERA)
		return (get_camera_pos(e, file, i));
	else
		return (get_obj_pos(e, file, i));
}
