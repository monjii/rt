/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_recursive.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	init_recur(t_collision *clsn, t_vec *self_color, double ratio,
					t_vec *ret)
{
	self_color->x = (double)((unsigned char)(clsn->final_color >> 16));
	self_color->y = (double)((unsigned char)(clsn->final_color >> 8));
	self_color->z = (double)((unsigned char)(clsn->final_color));
	ret->x = ratio * self_color->x;
	ret->y = ratio * self_color->y;
	ret->z = ratio * self_color->z;
}

t_vec	asm_recur(t_collision *clsn)
{
	t_vec	self_color;
	t_vec	ret;
	double	r[3];
	t_vec	refract;
	t_vec	reflect;

	r[0] = (1.0 - clsn->object->transparency) * (1.0 - clsn->object->reflect);
	r[1] = clsn->object->transparency * (1.0 - clsn->object->reflect);
	r[2] = clsn->object->reflect;
	init_recur(clsn, &self_color, r[0], &ret);
	if (clsn->refract != NULL)
	{
		refract = asm_recur(clsn->refract);
		ret.x += r[1] * refract.x;
		ret.y += r[1] * refract.y;
		ret.z += r[1] * refract.z;
	}
	if (clsn->reflect != NULL)
	{
		reflect = asm_recur(clsn->reflect);
		ret.x += r[2] * reflect.x;
		ret.y += r[2] * reflect.y;
		ret.z += r[2] * reflect.z;
	}
	return (ret);
}

int		asm_colors(t_collision *clsn)
{
	t_vec	ret;

	ret = asm_recur(clsn);
	return (construct_color(ret.x, ret.y, ret.z));
}
