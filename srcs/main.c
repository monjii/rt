/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

//void	screenshot(char *data)
//{
//	int fd;
//
//	fd = open("screenshot.bmp", O_CREAT | O_RDWR);
//	write(fd, data, SCR_Y * SCR_X * 4);
//	printf("%s\n", data);
//}

int		expose_hook(t_env *e)
{
	mlx_put_image_to_win(e);
	return (0);
}

int		mouse_hook(int keycode, int x, int y, t_env *e)
{
	int		index;

	if (keycode && x >= 0 && y >= 0)
	{
		index = y * e->sl + x * 4;
		e->object_selected = e->img_data[index + 3];
		printf("object_selected %d\n", e->object_selected);
	}
	return (0);
}

void	keycode_handler(t_env *e, int keycode)
{
	if (keycode == KEY_U)
		e->camera->pos.z += 1.0;
	if (keycode == KEY_D)
		e->camera->pos.z -= 1.0;
	if (keycode == KEY_L)
		e->camera->pos.x -= 1.0;
	if (keycode == KEY_R)
		e->camera->pos.x += 1.0;
	if (keycode == KEY_PLUS)
		e->camera->pos.y -= 1.0;
	if (keycode == KEY_MINUS)
		e->camera->pos.y += 1.0;
	if (keycode == 91)
		e->camera->rot.x += 0.2;
	if (keycode == 84)
		e->camera->rot.x -= 0.2;
	if (keycode == 88)
		e->camera->rot.y += 0.2;
	if (keycode == 86)
		e->camera->rot.y -= 0.2;
	if (keycode == KEY_NB)
		e->nb = (e->nb == 0 ? 1 : 0);
	if (keycode == KEY_CARTOON)
		e->cartoon = (e->cartoon == 0 ? 1 : 0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
		quit_program("End of program", e);
	keycode_handler(e, keycode);
	ft_bzero(e->img_data_aa, SCR_X * SCR_Y * e->aa * e->aa * 4);
	tracee(e);
	mlx_put_image_to_win(e);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env		*e;

	e = (t_env *)malloc(sizeof(t_env));
	init(e);
	parse_file(e, argc, argv[1]);
	printf("Parse achieved\n");
	init_objs(e);
	if ((e->mlx_ptr = mlx_init()) == NULL)
		return (1);
	if ((e->win_ptr = mlx_new_window(e->mlx_ptr, SCR_X, SCR_Y, "Ray Tracer"))
		== NULL)
		return (1);
	srand(time(NULL));
	e->img_ptr = mlx_new_image(e->mlx_ptr, SCR_X, SCR_Y);
	e->img_data = mlx_get_data_addr(e->img_ptr, &(e->bpp), &(e->sl), &(e->end));
	e->img_data_aa = (unsigned char *)malloc(sizeof(unsigned char) * SCR_X *
		SCR_Y * e->aa * e->aa * 4);
	tracee(e);
	mlx_expose_hook(e->win_ptr, expose_hook, e);
	mlx_key_hook(e->win_ptr, key_hook, e);
	mlx_mouse_hook(e->win_ptr, mouse_hook, e);
	mlx_loop(e->mlx_ptr);
	return (0);
}
