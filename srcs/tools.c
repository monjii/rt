/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	usage(char *error)
{
	ft_putendl(error);
	exit(0);
}

void	ft_blue(char *s)
{
	ft_putstr(ANSI_COLOR_BLUE);
	ft_putendl(s);
}

void	ft_error(char *s, int fd)
{
	ft_putstr_fd(ANSI_COLOR_RED, fd);
	ft_putendl_fd(s, fd);
	exit(EXIT_FAILURE);
}

void	quit_program(char *str, t_env *e)
{
	mlx_destroy_image(e->mlx_ptr, e->img_ptr);
	mlx_destroy_window(e->mlx_ptr, e->win_ptr);
	usage(str);
}

void	free_clsn(t_collision *clsn)
{
	if (clsn->reflect != NULL)
		free_clsn(clsn->reflect);
	if (clsn->refract != NULL)
		free_clsn(clsn->refract);
	free(clsn);
	clsn = NULL;
}
