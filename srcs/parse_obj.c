/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_obj.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		master_piece(t_env *e)
{
	add_obj(e, PARABOLOID);
	return (1);
}

int		its_obj(t_env *e, char **file, int *type)
{
	if (ft_strcmp(file[0], "sphere") == 0 && (*type = SPHERE))
	{
		add_obj(e, SPHERE);
		return (1);
	}
	else if (ft_strcmp(file[0], "plane") == 0 && (*type = PLANE))
	{
		add_obj(e, PLANE);
		return (1);
	}
	else if (ft_strcmp(file[0], "cylinder") == 0 && (*type = CYLINDER))
	{
		add_obj(e, CYLINDER);
		return (1);
	}
	else if (ft_strcmp(file[0], "cone") == 0 && (*type = CONE))
	{
		add_obj(e, CONE);
		return (1);
	}
	else if (ft_strcmp(file[0], "paraboloid") == 0 && (*type = PARABOLOID))
		return (master_piece(e));
	return (0);
}

int		isnt_obj(t_env *e, char **file, int *type)
{
	if (ft_strcmp(file[0], "camera") == 0 && (*type = CAMERA))
		return (1);
	else if (ft_strcmp(file[0], "filter") == 0)
		return (1);
	else if (ft_strcmp(file[0], "antialiasing") == 0)
		return (1);
	else if (ft_strcmp(file[0], "ambiant") == 0)
		return (1);
	else if (ft_strcmp(file[0], "light") == 0 && (*type = LIGHT))
	{
		add_light(e);
		return (1);
	}
	return (0);
}

int		check_if_obj(t_env *e, char **file, int *type)
{
	int		count;
	int		count2;

	count = 0;
	count2 = 0;
	if (file[0] != NULL)
	{
		count = its_obj(e, file, type);
		if (count == 0)
			count2 = isnt_obj(e, file, type);
		if (count == 0 && count2 == 0)
			ft_error("error: You have put an invalid object", 2);
	}
	else
		ft_error("error: no object found in the line", 2);
	return (0);
}
