/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectors_ray.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

t_vec	reflect_ray(t_vec *ray, t_vec *normale)
{
	t_vec	reflect;

	reflect.x = 2.0 * dot(normale, ray) * normale->x;
	reflect.y = 2.0 * dot(normale, ray) * normale->y;
	reflect.z = 2.0 * dot(normale, ray) * normale->z;
	reflect = sub_vec(ray, &reflect);
	reflect = normalize(&reflect);
	return (reflect);
}

t_vec	refract_ray(t_vec *ray, t_vec *normale, double eta)
{
	t_vec	refract;
	double	k;

	k = 1.0 - eta * eta * (1.0 - dot(normale, ray) * dot(normale, ray));
	if (k < 0.0)
		refract = reflect_ray(ray, normale);
	else
	{
		refract.x = eta * ray->x - (eta * dot(normale, ray) + sqrt(k)) *
					normale->x;
		refract.y = eta * ray->y - (eta * dot(normale, ray) + sqrt(k)) *
					normale->y;
		refract.z = eta * ray->z - (eta * dot(normale, ray) + sqrt(k)) *
					normale->z;
	}
	refract = normalize(&refract);
	return (refract);
}
