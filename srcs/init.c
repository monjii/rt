/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

t_collision		*init_clsn(t_env *e)
{
	t_collision	*clsn;

	if (!(clsn = malloc(sizeof(t_collision))))
		ft_error("error: Clsn malloc failed in init.c", 2);
	clsn->spec_ratio = 0.0;
	clsn->impact_d = -1.0;
	clsn->ray.origin = e->camera->pos;
	clsn->depth = 0;
	clsn->object = NULL;
	clsn->refract = NULL;
	clsn->reflect = NULL;
	clsn->prev = NULL;
	clsn->shadow_impact = -1.0;
	return (clsn);
}

void			init_objs(t_env *e)
{
	t_obj	*ptr;
	t_light	*ptr2;

	ptr = e->obj;
	while (ptr != NULL)
	{
		ptr->dir = normalize(&ptr->dir);
		rotate(&ptr->dir, ptr->rot.x, ptr->rot.y, ptr->rot.z);
		ptr = ptr->next;
	}
	ptr2 = e->light;
	while (ptr2 != NULL)
	{
		ptr2->dir = normalize(&ptr2->dir);
		rotate(&ptr2->dir, ptr2->rot.x, ptr2->rot.y, ptr2->rot.z);
		ptr2 = ptr2->next;
	}
	e->camera->dir = normalize(&e->camera->dir);
}

void			init(t_env *e)
{
	e->ambiant_light = 0.1;
	e->object_selected = 0;
	e->camera = (t_camera *)malloc(sizeof(t_camera));
	set_vec(&e->camera->pos, 0.0, 0.0, 0.0);
	set_vec(&e->camera->rot, 0.0, 0.0, 0.0);
	set_vec(&e->camera->dir, 0.0, 0.0, 0.0);
	e->ray = (t_ray *)malloc(sizeof(t_ray));
	e->ray->origin.x = e->camera->pos.x;
	e->ray->origin.y = e->camera->pos.y;
	e->ray->origin.z = e->camera->pos.z;
	e->nbr_lights = 0;
	e->obj = NULL;
	e->light = NULL;
	e->aa = 1;
	e->nb = 0;
	e->obj_id = 0;
	e->cartoon = 0;
}
