/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_loop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		func1(t_env *e, char **file, int type, int *i)
{
	int		tmp;

	tmp = *i;
	if (file[*i] != NULL && ft_strcmp(file[*i], "position") == 0)
		*i += get_position(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "rotate") == 0)
		*i += get_rotate(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "radius") == 0)
		*i += get_radius(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "color") == 0)
		*i += get_color_value(e, file, *i, type);
	if (tmp != *i)
		return (1);
	return (0);
}

int		func2(t_env *e, char **file, int type, int *i)
{
	int		tmp;

	tmp = *i;
	if (file[*i] != NULL && (ft_strcmp(file[*i], "slide") == 0))
		*i += get_slide(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "reflect") == 0)
		*i += get_reflect(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "refract") == 0)
		*i += get_refract(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[*i], "texture") == 0)
		*i += get_texture(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[0], "ambiant") == 0)
	{
		ambiant(e, file);
		return (1);
	}
	if (tmp != *i)
		return (1);
	return (0);
}

int		func3(t_env *e, char **file, int type, int *i)
{
	int		tmp;

	tmp = *i;
	(void)type;
	if (file[*i] != NULL && ft_strcmp(file[*i], "direction") == 0)
		*i += get_dir(e, file, *i, type);
	else if (file[*i] != NULL && ft_strcmp(file[0], "light") == 0 &&
				ft_strcmp(file[*i], "type") == 0)
		*i += get_type(e, file, *i);
	if (tmp != *i)
		return (1);
	return (0);
}

void	func4(t_env *e, char **file, int *i)
{
	if (file[*i] != NULL && ft_strcmp(file[0], "antialiasing") == 0 && (*i++))
		antialiasing(e, file);
	else if (file[*i] != NULL && ft_strcmp(file[0], "ambiant") == 0)
		ambiant(e, file);
	else if (file[*i] != NULL && ft_strcmp(file[0], "light") &&
				ft_strcmp(file[*i], "type") == 0)
		get_type(e, file, *i);
}

void	get_info(t_env *e, char *line)
{
	int		i;
	int		j;
	char	**file;
	int		type;
	int		*error;

	i = 1;
	j = -1;
	type = 0;
	error = ft_memalloc(sizeof(int) * 3);
	file = ft_strsplit(line, '	');
	check_if_obj(e, file, &type);
	default_init(e, type, error);
	func4(e, file, &i);
	while (file[i])
	{
		error[0] = func1(e, file, type, &i);
		error[1] = func2(e, file, type, &i);
		error[2] = func3(e, file, type, &i);
		while (error[++j])
			if (error[j] == 0)
				ft_error("error: You have put some invalid information.", 2);
		i++;
	}
}
