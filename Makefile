# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/13 16:28:30 by qjacob            #+#    #+#              #
#    Updated: 2016/07/07 15:58:11 by qjacob           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rt

SRC =	srcs/antialiasing.c\
		srcs/color_recursive.c\
		srcs/features.c\
		srcs/impact.c\
		srcs/init.c\
		srcs/light.c\
		srcs/list.c\
		srcs/main.c\
		srcs/math.c\
		srcs/math_primitives.c\
		srcs/matrix.c\
		srcs/noise.c\
		srcs/normale.c\
		srcs/parse.c\
		srcs/parse_loop.c\
		srcs/parse_features.c\
		srcs/parse_color.c\
		srcs/parse_camera.c\
		srcs/parse_dir.c\
		srcs/parse_get.c\
		srcs/parse_get2.c\
		srcs/parse_hexa.c\
		srcs/parse_light.c\
		srcs/parse_obj.c\
		srcs/parse_refl.c\
		srcs/parse_slide.c\
		srcs/parse_init.c\
		srcs/parse_tex.c\
		srcs/primitives.c\
		srcs/tools.c\
		srcs/tools_v2.c\
		srcs/trace.c\
		srcs/vectors.c\
		srcs/vectors_ray.c

OBJ = $(SRC:.c=.o)

%.o:%.c
	@echo " - Creating $<..."
	@$(CC) $(CFLAGS) $(INC) -c $< -o $@

CFLAGS = -Wall -Wextra -Werror -Ofast -g

INC = -I include -I libft

#LIB = -L ./libft -lft -lmlx -framework OpenGL -framework AppKit -lncurses
LIB = -L ./libft -lft -L ../minilibx_macos -lmlx -framework OpenGL -framework AppKit -lncurses

CC = gcc

all: $(NAME)

$(NAME): $(OBJ)
	@make -C libft/
	@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIB)
	@echo "\033[1;32m"
	@echo "* ===================================================================================== *"
	@echo "║  ██████╗  █████╗ ██╗   ██╗    ████████╗██████╗  █████╗  ██████╗███████╗██████╗        ║"
	@echo "║  ██╔══██╗██╔══██╗╚██╗ ██╔╝    ╚══██╔══╝██╔══██╗██╔══██╗██╔════╝██╔════╝██╔══██╗       ║"
	@echo "║  ██████╔╝███████║ ╚████╔╝        ██║   ██████╔╝███████║██║     █████╗  ██████╔╝       ║"
	@echo "║  ██╔══██╗██╔══██║  ╚██╔╝         ██║   ██╔══██╗██╔══██║██║     ██╔══╝  ██╔══██╗       ║"
	@echo "║  ██║  ██║██║  ██║   ██║          ██║   ██║  ██║██║  ██║╚██████╗███████╗██║  ██║       ║"
	@echo "║  ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝          ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚══════╝╚═╝  ╚═╝       ║"
	@echo "║  Create by: jfuster, qjacob, jgirard                                                  ║"
	@echo "║  Usage: Run the program with ./rt [scene]                                             ║"
	@echo "║  Note: You can find the scene in scenes directory.                                    ║"
	@echo "* ===================================================================================== *"

clean:
	@echo " - Cleaning object files..."
	@make clean -C libft/
	@rm -f $(OBJ)

fclean: clean
	@echo " - Cleaning binary..."
	@make fclean -C libft/
	@rm -f $(NAME)

re: fclean all

.PHONY: clean fclean re
