/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   impact.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		impact(t_env *e, t_ray *ray, t_collision *clsn, int fill)
{
	int		impact;
	t_obj	*obj;

	impact = 0;
	obj = e->obj;
	while (obj != NULL)
	{
		if (obj->type == SPHERE)
			impact += impact_sphere(obj, ray, clsn, fill);
		if (obj->type == PLANE)
			impact += impact_plane(obj, ray, clsn, fill);
		if (obj->type == CYLINDER)
			impact += impact_cylinder(obj, ray, clsn, fill);
		if (obj->type == CONE)
			impact += impact_cone(obj, ray, clsn, fill);
		if (obj->type == PARABOLOID)
			impact += impact_paraboloid(obj, ray, clsn, fill);
		obj = obj->next;
	}
	return (impact > 0 ? 1 : 0);
}

int		have_impact(t_env *e, t_collision *clsn, int depth)
{
	int		coll;

	coll = 0;
	apply_light(e, clsn);
	clsn->final_color = get_color(e, clsn->object->color, clsn);
	coll = 1;
	if (clsn->object->reflect != 0.0 && depth + 1 < DEPTH)
		add_clsn(clsn, REFLECT);
	if (clsn->object->transparency != 0.0 && depth + 1 < DEPTH)
		add_clsn(clsn, REFRACT);
	return (coll);
}

void	del_last_node(t_collision *clsn)
{
	if (clsn->prev != NULL && clsn->prev->reflect == clsn)
	{
		clsn->prev->reflect = NULL;
		free(clsn);
	}
	if (clsn->prev != NULL && clsn->prev->refract == clsn)
	{
		clsn->prev->refract = NULL;
		free(clsn);
	}
}

int		impact_loop(t_env *e, int depth, t_collision *clsn)
{
	int		coll;

	coll = 0;
	if (impact(e, &clsn->ray, clsn, 1))
		coll = have_impact(e, clsn, depth);
	else
		del_last_node(clsn);
	if (coll > 0 && clsn->reflect != NULL && clsn->refract != NULL &&
		depth < DEPTH)
		return (1 + impact_loop(e, depth + 1, clsn->reflect) +
			impact_loop(e, depth + 1, clsn->refract));
	else if (coll > 0 && clsn->refract != NULL && depth < DEPTH)
		return (1 + impact_loop(e, depth + 1, clsn->refract));
	else if (coll > 0 && clsn->reflect != NULL && depth < DEPTH)
		return (1 + impact_loop(e, depth + 1, clsn->reflect));
	return (coll);
}
