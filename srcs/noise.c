/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   noise.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

static void	init_n2(t_noise *data, int i, int j)
{
	int		k;

	while (--i)
	{
		k = data->p[i];
		j = random() % B;
		data->p[i] = data->p[j];
		data->p[j] = k;
	}
	i = 0;
	while (i < B + 2)
	{
		data->p[B + i] = data->p[i];
		data->g1[B + i] = data->g1[i];
		j = -1;
		while (++j < 2)
			data->g2[B + i][j] = data->g2[i][j];
		j = -1;
		while (++j < 3)
			data->g3[B + i][j] = data->g3[i][j];
		i++;
	}
}

static void	init_n(t_noise *data)
{
	int i;
	int j;

	i = 0;
	while (i < B)
	{
		data->p[i] = i;
		data->g1[i] = (double)((random() % (B + B)) - B) / B;
		j = 0;
		while (j < 2)
		{
			data->g2[i][j] = (double)((random() % (B + B)) - B) / B;
			j++;
		}
		j = 0;
		while (j < 3)
		{
			data->g3[i][j] = (double)((random() % (B + B)) - B) / B;
			j++;
		}
		normalize3(data->g3[i]);
		i++;
	}
	init_n2(data, i, j);
}

void		setup(int *tab, double *tab2, double *vec)
{
	tab2[12] = vec[0] + N;
	tab[0] = ((int)tab2[12]) & BM;
	tab[1] = (tab[0] + 1) & BM;
	tab2[0] = tab2[12] - (int)tab2[12];
	tab2[1] = tab2[0] - 1.0;
	tab2[12] = vec[1] + N;
	tab[2] = ((int)tab2[12]) & BM;
	tab[3] = (tab[2] + 1) & BM;
	tab2[2] = tab2[12] - (int)tab2[12];
	tab2[3] = tab2[2] - 1.0;
	tab2[12] = vec[2] + N;
	tab[4] = ((int)tab2[12]) & BM;
	tab[5] = (tab[4] + 1) & BM;
	tab2[4] = tab2[12] - (int)tab2[12];
	tab2[5] = tab2[4] - 1.0;
}

void		perlin_noise2(t_noise *data, int *tab, double *tab2)
{
	data->q = data->g3[tab[6] + tab[4]];
	tab2[13] = AT3(tab2[0], tab2[2], tab2[4]);
	data->q = data->g3[tab[7] + tab[4]];
	tab2[14] = AT3(tab2[1], tab2[2], tab2[4]);
	tab2[8] = LERP(tab2[12], tab2[13], tab2[14]);
	data->q = data->g3[tab[8] + tab[4]];
	tab2[13] = AT3(tab2[0], tab2[3], tab2[4]);
	data->q = data->g3[tab[9] + tab[4]];
	tab2[14] = AT3(tab2[1], tab2[3], tab2[4]);
	tab2[9] = LERP(tab2[12], tab2[13], tab2[14]);
	tab2[10] = LERP(tab2[6], tab2[8], tab2[9]);
	data->q = data->g3[tab[6] + tab[5]];
	tab2[13] = AT3(tab2[0], tab2[2], tab2[5]);
	data->q = data->g3[tab[7] + tab[5]];
	tab2[14] = AT3(tab2[1], tab2[2], tab2[5]);
	tab2[8] = LERP(tab2[12], tab2[13], tab2[14]);
	data->q = data->g3[tab[8] + tab[5]];
	tab2[13] = AT3(tab2[0], tab2[3], tab2[5]);
	data->q = data->g3[tab[9] + tab[5]];
	tab2[14] = AT3(tab2[1], tab2[3], tab2[5]);
	tab2[9] = LERP(tab2[12], tab2[13], tab2[14]);
	tab2[11] = LERP(tab2[6], tab2[8], tab2[9]);
}

double		perlin_noise(double vec[3])
{
	static t_noise	*data;
	int				tab[10];
	double			tab2[15];
	int				i[2];
	static int		start = 1;

	if (start)
	{
		data = (t_noise *)malloc(sizeof(t_noise));
		start = 0;
		init_n(data);
	}
	setup(tab, tab2, vec);
	i[0] = data->p[tab[0]];
	i[1] = data->p[tab[1]];
	tab[6] = data->p[i[0] + tab[2]];
	tab[7] = data->p[i[1] + tab[2]];
	tab[8] = data->p[i[0] + tab[3]];
	tab[9] = data->p[i[1] + tab[3]];
	tab2[12] = S_CURVE(tab2[0]);
	tab2[6] = S_CURVE(tab2[2]);
	tab2[7] = S_CURVE(tab2[4]);
	perlin_noise2(data, tab, tab2);
	return (LERP(tab2[7], tab2[10], tab2[11]));
}
