/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_v2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

unsigned int	construct_color(unsigned char r, unsigned char g,
	unsigned char b)
{
	unsigned int		color;

	color = (b & 0x0000FF);
	color += ((g & 0x0000FF) << 8);
	color += ((r & 0x0000FF) << 16);
	return (color);
}

void			put_pixel_to_img(t_env *e, int x, int y, t_collision *clsn)
{
	int		index;
	int		color;

	index = y * e->sl * e->aa + x * 4;
	color = asm_colors(clsn);
	e->img_data_aa[index] += (color & 0x0000FF);
	e->img_data_aa[index + 1] += ((color & 0x00FF00) >> 8);
	e->img_data_aa[index + 2] += ((color & 0xFF0000) >> 16);
	e->img_data_aa[index + 3] = clsn->object->id;
}

void			normalize3(double v[3])
{
	double s;

	s = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	v[0] = v[0] / s;
	v[1] = v[1] / s;
	v[2] = v[2] / s;
}
