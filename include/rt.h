/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:53 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:37:55 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H
//# include <mlx.h>
# include "../../minilibx_macos/mlx.h"
# include "../libft/includes/libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <fcntl.h>
# include <pthread.h>
# include <signal.h>
# include <ncurses.h>
# include <stdio.h>

# define ANSI_COLOR_BLUE    "\033[34m"
# define ANSI_COLOR_RED     "\033[31m"

# define KEY_ESC	53
# define KEY_L		123
# define KEY_R		124
# define KEY_U		126
# define KEY_D		125
# define KEY_PLUS	69
# define KEY_MINUS	78
# define KEY_NB		45
# define KEY_CARTOON	8

# define SCR_X	1200
# define SCR_Y	800

# define HEIGHT 20
# define WIDTH 60

# define SPHERE		1
# define CONE		2
# define PLANE		3
# define CYLINDER	4
# define PARABOLOID	5
# define LIGHT		6
# define CAMERA		7

# define DEPTH			4
# define THREAD_LIMIT	4
# define REFLECT		0
# define REFRACT		1

# define PUNCTUAL			1
# define DIRECTIONAL		2
# define PARALLEL			3

# define ORIGIN			1
# define BLACKNWHITE	2
# define SEPIA			3

# define PERLIN			1
# define PERTURBATION	2

# define B				0x100
# define BM				0xff
# define N				0x1000
# define NP				12
# define NM				0xfff
# define S_CURVE(a)		(a * a * (3. - 2. * a))
# define LERP(a, c, b)	(c + a * (b - c))
# define AT3(rx,ry,rz)	(rx * data->q[0] + ry * data->q[1] + rz * data->q[2])

typedef struct	s_vec
{
	double				x;
	double				y;
	double				z;
}				t_vec;

typedef struct	s_camera
{
	t_vec				pos;
	t_vec				dir;
	t_vec				rot;
}				t_camera;

typedef struct	s_light
{
	t_vec				pos;
	t_vec				dir;
	t_vec				rot;
	int					color;
	int					type;

	struct s_light		*next;
	struct s_light		*last;
}				t_light;

typedef struct	s_ray
{
	t_vec				origin;
	t_vec				dir;
}				t_ray;

typedef struct	s_obj
{
	int					type;
	int					id;

	t_vec				pos;
	t_vec				dir;
	t_vec				rot;

	double				angle;
	double				radius;
	double				slide;

	int					color;
	double				transparency;
	int					texture;

	double				reflect;
	double				refract;

	struct s_obj		*next;
	struct s_obj		*last;
}				t_obj;

typedef struct	s_collision
{
	t_ray				ray;

	t_obj				*object;

	t_vec				impact;
	t_vec				normale;
	t_vec				reflect_ray;
	t_vec				refract_ray;
	double				impact_d;
	double				dist_tolight;

	double				light_ratio;
	double				spec_ratio;
	int					final_color;

	int					light_color;
	double				shadow_impact;

	int					depth;

	struct s_collision	*refract;
	struct s_collision	*reflect;
	struct s_collision	*prev;
}				t_collision;

typedef struct	s_env
{
	void				*img_ptr;
	char				*img_data;
	unsigned char		*img_data_aa;
	void				*mlx_ptr;
	void				*win_ptr;

	int					sl;
	int					bpp;
	int					end;

	double				ambiant_light;
	t_camera			*camera;
	t_light				*light;
	t_ray				*ray;
	t_obj				*obj;

	int					obj_id;
	int					nbr_lights;
	int					count_type;
	int					choice;
	int					fd[2];

	int					nb;
	int					aa;
	int					cartoon;
	int					object_selected;
	pthread_mutex_t		mutex;
	pid_t				pid;
}				t_env;

typedef struct	s_thread_data
{
	t_env				*e;

	int					y_start;
	int					y_end;
}				t_thread_data;

typedef struct	s_thr
{
	pthread_t			thr_id[THREAD_LIMIT];
}				t_thr;

typedef struct	s_noise
{
	double				*q;
	int					p[B + B + 2];
	double				g3[B + B + 2][3];
	double				g2[B + B + 2][2];
	double				g1[B + B + 2];
}				t_noise;

/*
**	antialiasing.c
*/
void			mlx_put_image_to_win(t_env *e);
unsigned char	get_aa_color(t_env *e, int x, int y, int offset);

/*
**	color_recursive.c
*/
int				asm_colors(t_collision *clsn);

/*
**	features.c
*/
t_vec			normale_perturbation(t_vec normale);
int				apply_nb(int color);
int				add_spec_to_color(unsigned char r, unsigned char g,
									unsigned char b, t_collision *clsn);
double			apply_noise(t_collision *clsn);
unsigned char	apply_noise_to_color(unsigned char color, double noise);

/*
**	impact.c
*/
int				impact(t_env *e, t_ray *ray, t_collision *clsn, int fill);
int				impact_loop(t_env *e, int depth, t_collision *clsn);

/*
**	init.c
*/
t_collision		*init_clsn(t_env *e);
void			init_objs(t_env *e);
void			init(t_env *e);

/*
**	light.c
*/
double			apply_specular(t_env *e, t_light *light, t_collision *clsn);
double			apply_diffuse(t_env *e, t_light *light, t_collision *clsn);
void			apply_light(t_env *e, t_collision *clsn);

/*
**	list.c
*/
void			add_obj(t_env *e, int type);
void			add_light(t_env *e);
void			add_clsn(t_collision *clsn, int action);

/*
**	main.c
*/
int				expose_hook(t_env *e);
void			keycode_handler(t_env *e, int keycode);
int				key_hook(int keycode, t_env *e);
int				mouse_hook(int keycode, int x, int y, t_env *e);

/*
**	math.c
*/
double			quadratic(double tab[3]);
void			fill_collision(t_obj *ptr, double dist, t_collision *clsn,
								t_ray *ray);

/*
**	math_primitives.c
*/
void			math_paraboloid(double *res, t_ray *ray, t_obj *ptr);
void			math_sphere(double *res, t_ray *ray, t_obj *ptr);
void			math_cylinder(double *res, t_ray *ray, t_obj *ptr);
void			math_cone(double *res, t_ray *ray, t_obj *ptr);


/*
**	matrix.c
*/
void			rotate(t_vec *vec, double rot_x, double rot_y, double rot_z);

/*
**	normale.c
*/
t_vec			calculate_normale(t_collision *clsn, int shape, t_ray *ray);

/*
**	parse.c
*/
void			parse_file(t_env *e, int ac, char *av);

/*
**	parse_loop.c
*/
int				func1(t_env *e, char **file, int type, int *i);
int				func2(t_env *e, char **file, int type, int *i);
int				func3(t_env *e, char **file, int type, int *i);
void			func4(t_env *e, char **file, int *i);
void			get_info(t_env *e, char *line);

int 	is_pos(char *str);

/*
**	parse_obj.c
*/
int 	check_if_obj(t_env *e, char **file, int *type);
int		check_camera_pos(char **file, int i, int *count_error);
int		check_rotate_info(char **file, int i, int *count_error);

void	default_init(t_env *e, int type, int *error);
void	set_cam(t_env *e);

int 	get_color_value(t_env *e, char **file, int i, int type);

int 	get_refract(t_env *e, char **file, int i, int type);
int 	get_reflect(t_env *e, char **file, int i, int type);
int		get_texture(t_env *e, char **file, int i, int type);

int 	hexa_six(int true_color, char *tmp_color);
int 	hexa_eight(int true_color, char *tmp_color);
int 	ambiant(t_env *e, char **file);

int 	antialiasing(t_env *e, char **file);

int 	get_rotate(t_env *e, char **file, int i, int type);
int 	get_position(t_env *e, char **file, int i, int type);
int 	get_radius(t_env *e, char **file, int i, int type);
int 	get_slide(t_env *e, char **file, int i, int type);
int 	get_dir(t_env *e, char **file, int i, int type);
int 	get_type(t_env *e, char **file, int i);

int 	light_dir(t_env *e, int *count_error, char **file, int i);
int 	get_light_pos(t_env *e, char **file, int i);

int		camerax_or_not(t_env *e, char **file, int i, int type);
int		cameray_or_not(t_env *e, char **file, int i, int type);
int		cameraz_or_not(t_env *e, char **file, int i, int type);


/*
**	primitives.c
*/
int				impact_sphere(t_obj *ptr, t_ray *ray, t_collision *clsn,
								int fill);
int				impact_plane(t_obj *ptr, t_ray *ray, t_collision *clsn,
								int fill);
int				impact_cylinder(t_obj *ptr, t_ray *ray, t_collision *clsn,
								int fill);
int				impact_cone(t_obj *ptr, t_ray *ray, t_collision *clsn,
								int fill);
int				impact_paraboloid(t_obj *ptr, t_ray *ray, t_collision *clsn,
								int fill);

/*
**	tools.c
*/
void			usage(char *error);
void			ft_blue(char *s);
void			ft_error(char *s, int fd);
void			quit_program(char *str, t_env *e);
void			free_clsn(t_collision *clsn);

/*
**	tools_v2.c
*/
unsigned int	construct_color(unsigned char r, unsigned char g,
								unsigned char b);
void			put_pixel_to_img(t_env *e, int x, int y, t_collision *clsn);
void			normalize3(double v[3]);

/*
**	trace.c
*/
int				get_color(t_env *e, int color, t_collision *clsn);
t_vec			get_ray_dir(t_env *e, int x, int y);
void			draw_pixel(t_env *e, t_collision *clsn);
void			tracee(t_env *e);

/*
**	vectors.c
*/
void			set_vec(t_vec *vec, double x, double y, double z);
double			magnitude(t_vec *vec);
t_vec			normalize(t_vec *vec);
t_vec			sub_vec(t_vec *vec1, t_vec *vec2);
double			dot(t_vec *a, t_vec *b);

/*
**	vectors_ray.c
*/
t_vec			reflect_ray(t_vec *ray, t_vec *normale);
t_vec			refract_ray(t_vec *ray, t_vec *normale, double eta);

double			perlin_noise(double vec[3]);

#endif
