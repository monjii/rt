/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	rotate_x(t_vec *vec, double rot)
{
	double	old_y;
	double	old_z;

	old_y = vec->y;
	old_z = vec->z;
	vec->y = (old_y * cos(rot)) + (old_z * (-sin(rot)));
	vec->z = (old_y * sin(rot)) + (old_z * cos(rot));
}

void	rotate_y(t_vec *vec, double rot)
{
	double	old_x;
	double	old_z;

	old_x = vec->x;
	old_z = vec->z;
	vec->x = (old_x * cos(rot)) + (old_z * sin(rot));
	vec->z = (old_x * (-sin(rot))) + (old_z * cos(rot));
}

void	rotate_z(t_vec *vec, double rot)
{
	double	old_x;
	double	old_y;

	old_x = vec->x;
	old_y = vec->y;
	vec->x = (old_x * cos(rot)) + (old_y * (-sin(rot)));
	vec->y = (old_x * sin(rot)) + (old_y * cos(rot));
}

void	rotate(t_vec *vec, double rot_x, double rot_y, double rot_z)
{
	if (rot_x != 0.0)
		rotate_x(vec, rot_x);
	if (rot_y != 0.0)
		rotate_y(vec, rot_y);
	if (rot_z != 0.0)
		rotate_z(vec, rot_z);
}
