/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_hexa.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		hexa_eight(int true_color, char *tmp_color)
{
	true_color += tmp_color[7];
	true_color += tmp_color[6] << 4;
	true_color += tmp_color[5] << 8;
	true_color += tmp_color[4] << 12;
	true_color += tmp_color[3] << 16;
	true_color += tmp_color[2] << 20;
	true_color += tmp_color[1] << 24;
	true_color += tmp_color[0] << 28;
	return (true_color);
}

int		hexa_six(int true_color, char *tmp_color)
{
	true_color += tmp_color[5];
	true_color += tmp_color[4] << 4;
	true_color += tmp_color[3] << 8;
	true_color += tmp_color[2] << 12;
	true_color += tmp_color[1] << 16;
	true_color += tmp_color[0] << 20;
	return (true_color);
}
