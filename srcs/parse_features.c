/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_features.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		is_pos(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' &&
			str[i] <= 'Z'))
			return (1);
		i++;
	}
	return (0);
}

int		check_ambiant_info(t_env *e, char **file, int *count_error)
{
	double		tmp;

	if (file[1] == NULL)
	{
		e->ambiant_light = 0.10;
		count_error[1] = 1;
		ft_blue("warning: You haven't put ambiant_light value, please enter \
				value");
	}
	else
	{
		tmp = ft_atof(file[1]);
		if (tmp < 0.0 || tmp > 0.50)
		{
			e->ambiant_light = 0.10;
			count_error[1] = 1;
			ft_blue("warning: You have put an incorrect value for \
					ambiant_light light. Format [0-1]");
		}
	}
	return (count_error[2] = 1);
}

int		ambiant(t_env *e, char **file)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 3);
	check_ambiant_info(e, file, count_error);
	if (count_error[1] != 1)
		e->ambiant_light = ft_atof(file[1]);
	return (count_error[2]);
}

int		check_aa_info(t_env *e, char **file, int *count_error)
{
	int		tmp;

	if (file[1] == NULL)
	{
		count_error[1] = 1;
		e->aa = 1;
		ft_blue("warning: You haven't put antialiasing value, please enter \
				1 | 2 | 3 value");
		return (count_error[2] = 0);
	}
	else
	{
		tmp = ft_atoi(file[1]);
		if (tmp != 1 && tmp != 2 && tmp != 3)
		{
			count_error[1] = 1;
			if (tmp < 1)
				e->aa = 1;
			else if (tmp > 3)
				e->aa = 3;
			return (count_error[2] = 1);
		}
	}
	return (count_error[3] = 3);
}

int		antialiasing(t_env *e, char **file)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 3);
	check_aa_info(e, file, count_error);
	if (count_error[1] != 1)
		e->aa = ft_atoi(file[1]);
	return (count_error[2]);
}
