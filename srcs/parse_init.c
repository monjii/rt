/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_init.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void	set_cam(t_env *e)
{
	set_vec(&e->camera->pos, 0.0, 0.0, 0.0);
	set_vec(&e->camera->rot, 0.0, 0.0, 0.0);
}

void	default_init(t_env *e, int type, int *error)
{
	ft_bzero(error, sizeof(int) * 3);
	if (type == CAMERA)
		set_cam(e);
	else if (type == LIGHT)
	{
		set_vec(&e->light->last->pos, 0.0, -5.0, 0.0);
		set_vec(&e->light->last->dir, 0.0, 0.0, 1.0);
		set_vec(&e->light->last->rot, 0.0, 0.0, 0.0);
		e->light->last->color = 0xFFFFFF;
		e->light->last->type = PUNCTUAL;
	}
	else if (type != 0)
	{
		set_vec(&e->obj->last->pos, 0.0, 0.0, 10.0);
		set_vec(&e->obj->last->rot, 0.0, 0.0, 0.0);
		set_vec(&e->obj->last->dir, 0.0, 1.0, 0.0);
		e->obj->last->radius = 5.0;
		e->obj->last->angle = 0.4;
		e->obj->last->texture = 0;
		e->obj->last->slide = 1.0;
		e->obj->last->color = 0xFFFFFF;
		e->obj->last->reflect = 0.0;
		e->obj->last->refract = 1.0;
		e->obj->last->transparency = 0.0;
	}
}
