/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_camera.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		camerax_or_not(t_env *e, char **file, int i, int type)
{
	if (type == CAMERA)
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->camera->rot.x = ft_atof(file[i + 1]);
	}
	else
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->obj->last->rot.x = ft_atof(file[i + 1]);
	}
	return (0);
}

int		cameray_or_not(t_env *e, char **file, int i, int type)
{
	if (type == CAMERA)
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->camera->rot.x = ft_atof(file[i + 1]);
	}
	else
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->obj->last->rot.x = ft_atof(file[i + 1]);
	}
	return (0);
}

int		cameraz_or_not(t_env *e, char **file, int i, int type)
{
	if (type == CAMERA)
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->camera->rot.x = ft_atof(file[i + 1]);
	}
	else
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->obj->last->rot.x = ft_atof(file[i + 1]);
	}
	return (0);
}
