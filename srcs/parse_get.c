/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_get.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		check_rotate_info(char **file, int i, int *count_error)
{
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
		return (count_error[3] = 1);
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
		return (count_error[3] = 2);
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot a value. Default\
				value was init at 0.0");
		return (count_error[3] = 3);
	}
	return (count_error[3] = 3);
}

int		check_camera_pos(char **file, int i, int *count_error)
{
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 1);
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 2);
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 3);
	}
	return (count_error[3] = 3);
}

int		get_radius_value(t_env *e, char **file, int i, int *count_error)
{
	if (file[i + 1] == NULL && (e->obj->last->radius = 5.0))
	{
		count_error[0] = 1;
		ft_blue("warning: You have forgot one or more value");
	}
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->obj->last->radius = ft_atof(file[i + 1]);
	}
	return (count_error[2] = 1);
}

int		get_radius(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 3);
	count_error[1] = type;
	if (type == SPHERE || type == CYLINDER)
		get_radius_value(e, file, i, count_error);
	else
	{
		e->obj->last->radius = 0.0;
		ft_blue("warning: you try to put radius value in obj who don't need.");
	}
	return (count_error[3] = 1);
}
