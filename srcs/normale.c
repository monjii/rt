/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normale.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

t_vec	plane_normale(t_collision *clsn)
{
	t_vec	normale;

	normale.x = clsn->object->pos.x;
	normale.y = clsn->object->pos.y;
	normale.z = clsn->object->pos.z;
	return (normale);
}

t_vec	cylinder_normale(t_collision *clsn, t_ray *ray)
{
	double	m;
	t_vec	pt;
	t_vec	normale;

	pt = sub_vec(&ray->origin, &clsn->object->pos);
	m = dot(&ray->dir, &clsn->object->dir) *
	clsn->impact_d + dot(&pt, &clsn->object->dir);
	normale.x = clsn->impact.x - clsn->object->pos.x - clsn->object->dir.x * m;
	normale.y = clsn->impact.y - clsn->object->pos.y - clsn->object->dir.y * m;
	normale.z = clsn->impact.z - clsn->object->pos.z - clsn->object->dir.z * m;
	return (normale);
}

t_vec	cone_normale(t_collision *clsn, t_ray *ray)
{
	double	m;
	t_vec	pt;
	t_vec	normale;

	pt = sub_vec(&ray->origin, &clsn->object->pos);
	m = dot(&ray->dir, &clsn->object->dir) *
	clsn->impact_d + dot(&pt, &clsn->object->dir);
	normale.x = clsn->impact.x - clsn->object->pos.x -
	pow(clsn->object->angle, 2.0) * clsn->object->dir.x * m;
	normale.y = clsn->impact.y - clsn->object->pos.y -
	pow(clsn->object->angle, 2.0) * clsn->object->dir.y * m;
	normale.z = clsn->impact.z - clsn->object->pos.z -
	pow(clsn->object->angle, 2.0) * clsn->object->dir.z * m;
	return (normale);
}

t_vec	paraboloid_normale(t_collision *clsn, t_ray *ray)
{
	double	m;
	t_vec	pt;
	t_vec	normale;

	pt = sub_vec(&ray->origin, &clsn->object->pos);
	m = dot(&ray->dir, &clsn->object->dir) *
	clsn->impact_d + dot(&pt, &clsn->object->dir);
	normale.x = clsn->impact.x - clsn->object->pos.x - clsn->object->dir.x *
					(m + clsn->object->slide);
	normale.y = clsn->impact.y - clsn->object->pos.y - clsn->object->dir.y *
					(m + clsn->object->slide);
	normale.z = clsn->impact.z - clsn->object->pos.z - clsn->object->dir.z *
					(m + clsn->object->slide);
	return (normale);
}

t_vec	calculate_normale(t_collision *clsn, int shape, t_ray *ray)
{
	t_vec		normale;

	if (shape == SPHERE)
	{
		normale.x = clsn->impact.x - clsn->object->pos.x;
		normale.y = clsn->impact.y - clsn->object->pos.y;
		normale.z = clsn->impact.z - clsn->object->pos.z;
	}
	else if (shape == PLANE)
		normale = plane_normale(clsn);
	else if (shape == CYLINDER)
		normale = cylinder_normale(clsn, ray);
	else if (shape == CONE)
		normale = cone_normale(clsn, ray);
	else
		normale = paraboloid_normale(clsn, ray);
	if (clsn->object->texture == PERTURBATION)
		normale = normale_perturbation(normale);
	normale = normalize(&normale);
	return (normale);
}
