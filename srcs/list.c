/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

void			add_obj(t_env *e, int type)
{
	t_obj		*ptr;
	t_obj		*new_obj;

	new_obj = (t_obj *)malloc(sizeof(t_obj));
	if (new_obj == NULL)
		usage("Malloc error");
	new_obj->next = NULL;
	new_obj->type = type;
	new_obj->id = e->obj_id;
	e->obj_id += 1;
	ptr = e->obj;
	if (e->obj == NULL)
	{
		e->obj = new_obj;
		e->obj->last = new_obj;
	}
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_obj;
		e->obj->last = new_obj;
	}
}

void			add_light(t_env *e)
{
	t_light		*ptr;
	t_light		*new_light;

	e->nbr_lights += 1;
	new_light = (t_light *)malloc(sizeof(t_light));
	if (new_light == NULL)
		usage("Malloc error");
	new_light->next = NULL;
	ptr = e->light;
	if (e->light == NULL)
	{
		e->light = new_light;
		e->light->last = new_light;
	}
	else
	{
		while (ptr->next != NULL)
			ptr = ptr->next;
		ptr->next = new_light;
		e->light->last = new_light;
	}
}

t_collision		*malloc_node(t_collision *old_clsn)
{
	t_collision		*new_clsn;

	new_clsn = (t_collision *)malloc(sizeof(t_collision));
	if (new_clsn == NULL)
		usage("Malloc error");
	new_clsn->object = NULL;
	new_clsn->refract = NULL;
	new_clsn->reflect = NULL;
	new_clsn->prev = old_clsn;
	new_clsn->depth = old_clsn->depth + 1;
	return (new_clsn);
}

void			add_clsn(t_collision *clsn, int action)
{
	t_collision		*new_clsn;

	new_clsn = malloc_node(clsn);
	if (action == REFLECT)
	{
		clsn->reflect = new_clsn;
		new_clsn->ray.dir = new_clsn->prev->reflect_ray;
		new_clsn->ray.origin.x = new_clsn->prev->impact.x +
									(0.00001 * new_clsn->ray.dir.x);
		new_clsn->ray.origin.y = new_clsn->prev->impact.y +
									(0.00001 * new_clsn->ray.dir.y);
		new_clsn->ray.origin.z = new_clsn->prev->impact.z +
									(0.00001 * new_clsn->ray.dir.z);
	}
	else if (action == REFRACT)
	{
		clsn->refract = new_clsn;
		new_clsn->ray.dir = clsn->refract_ray;
		new_clsn->ray.origin.x = clsn->impact.x + (0.00001 * clsn->ray.dir.x);
		new_clsn->ray.origin.y = clsn->impact.y + (0.00001 * clsn->ray.dir.y);
		new_clsn->ray.origin.z = clsn->impact.z + (0.00001 * clsn->ray.dir.z);
	}
	new_clsn->spec_ratio = 0.0;
	clsn->shadow_impact = -1.0;
	new_clsn->impact_d = -1.0;
}
