/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_dir.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		check_dir(t_env *e, char **file, int i, int *count_error)
{
	(void)e;
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 1);
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 2);
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 3);
	}
	return (count_error[3] = 3);
}

int		obj_dir(t_env *e, int *count_error, char **file, int i)
{
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->obj->last->dir.x = ft_atof(file[i + 1]);
	}
	if (count_error[1] != 1)
	{
		if (is_pos(file[i + 2]) == 1)
			return (count_error[3] = 1);
		e->obj->last->dir.y = ft_atof(file[i + 2]);
	}
	if (count_error[2] != 1)
	{
		if (is_pos(file[i + 3]) == 1)
			return (count_error[3] = 1);
		e->obj->last->dir.z = ft_atof(file[i + 3]);
	}
	return (0);
}

int		get_dir(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 3);
	check_dir(e, file, i, count_error);
	if (type == LIGHT)
		return (light_dir(e, count_error, file, i));
	else
		return (obj_dir(e, count_error, file, i));
	return (count_error[3]);
}
