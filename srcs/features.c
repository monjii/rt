/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   features.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

t_vec			normale_perturbation(t_vec normale)
{
	t_vec	new_vec;

	new_vec = normale;
	new_vec.x *= (sinf(normale.y) + cosf(normale.z));
	new_vec.y *= (sinf(normale.z) + cosf(new_vec.x));
	new_vec.z *= (sinf(new_vec.x) + cosf(new_vec.y));
	return (new_vec);
}

int				apply_nb(int color)
{
	int				nb_color;
	int				tone;
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;

	r = (color >> 16);
	g = (color >> 8);
	b = color;
	tone = r + g + b;
	tone /= 3;
	r = tone;
	g = tone;
	b = tone;
	nb_color = construct_color(r, g, b);
	return (nb_color);
}

double			apply_noise(t_collision *clsn)
{
	double	noise;
	double	vec[3];

	vec[0] = clsn->impact.x;
	vec[1] = clsn->impact.y;
	vec[2] = clsn->impact.y;
	noise = 40.0 * perlin_noise(vec);
	noise = noise - (double)((int)(noise));
	return (noise);
}

unsigned char	apply_noise_to_color(unsigned char color, double noise)
{
	if (noise >= 0.0)
		color = color * noise;
	else
		color = color * (noise * -1.0);
	return (color);
}

int				add_spec_to_color(unsigned char r, unsigned char g,
									unsigned char b, t_collision *clsn)
{
	int		color;

	if (r + (int)(clsn->spec_ratio * 255.0) > 255)
		r = 255;
	else
		r += (int)(clsn->spec_ratio * 255.0);
	if (g + (int)(clsn->spec_ratio * 255.0) > 255)
		g = 255;
	else
		g += (int)(clsn->spec_ratio * 255.0);
	if (b + (int)(clsn->spec_ratio * 255.0) > 255)
		b = 255;
	else
		b += (int)(clsn->spec_ratio * 255.0);
	color = construct_color(r, g, b);
	return (color);
}
