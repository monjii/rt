/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diffuse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

double	apply_specular(t_env *e, t_light *light, t_collision *clsn)
{
	t_vec	light_dir;
	t_ray	sub;
	double	cos_s;

	light_dir = sub_vec(&light->pos, &clsn->impact);
	light_dir = normalize(&light_dir);
	// if (light->type == PUNCTUAL)
	cos_s = dot(&clsn->normale, &light_dir);
	// else
	// 	cos_s = dot(&clsn->normale, &light->dir);
	sub.origin = clsn->impact;
	sub.dir = light_dir;
	if (cos_s < 0.0)
		cos_s = 0.0;
	if (impact(e, &sub, clsn, 0))
		cos_s *= clsn->shadow_impact;
	return (cos_s);
}

double	apply_diffuse(t_env *e, t_light *light, t_collision *clsn)
{
	t_vec	light_dir;
	t_ray	sub;
	double	cos_s;

	light_dir = sub_vec(&light->pos, &clsn->impact);
	clsn->dist_tolight = magnitude(&light_dir);
	light_dir = normalize(&light_dir);
	// if (light->type == PUNCTUAL)
	cos_s = dot(&clsn->normale, &light_dir);
	// else
	// 	cos_s = dot(&clsn->normale, &light->dir);
	sub.origin = clsn->impact;
	sub.dir = light_dir;
	if (cos_s < 0.0)
		cos_s = 0.0;
	if (impact(e, &sub, clsn, 0))
		cos_s *= clsn->shadow_impact;
	return (cos_s);
}

void	apply_light(t_env *e, t_collision *clsn)
{
	double			light_cos;
	double			ret;
	t_vec			l_color;
	t_light			*lights;

	ret = 0.0;
	lights = e->light;
	set_vec(&l_color, 0.0, 0.0, 0.0);
	while (lights != NULL)
	{
		light_cos = apply_diffuse(e, lights, clsn);
		l_color.x += (unsigned char)(lights->color >> 16) * light_cos;
		l_color.y += (unsigned char)(lights->color >> 8) * light_cos;
		l_color.z += (unsigned char)(lights->color) * light_cos;
		ret = apply_specular(e, lights, clsn);
		if (ret > clsn->spec_ratio)
			clsn->spec_ratio = ret;
		lights = lights->next;
		clsn->shadow_impact = -1.0;
	}
	if (e->nbr_lights != 0)
		set_vec(&l_color, l_color.x / (double)e->nbr_lights, l_color.y /
			(double)e->nbr_lights, l_color.z / (double)e->nbr_lights);
	clsn->light_color = construct_color(l_color.x, l_color.y, l_color.z);
	clsn->spec_ratio = pow(clsn->spec_ratio, 70);
}
