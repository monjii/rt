/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_refl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		reflect_isnull(int *count_error, int type, t_env *e)
{
	count_error[0] = 1;
	ft_blue("warning: You have forgot one or more value");
	if (type == LIGHT && type == CAMERA)
	{
		ft_blue("warning: You try to put reflect to the light or camera.");
		return (1);
	}
	else
		e->obj->last->reflect = 0.0;
	return (0);
}

int		get_reflect(t_env *e, char **file, int i, int type)
{
	int		*count_error;
	int		tmp;

	count_error = ft_memalloc(sizeof(int) * 2);
	if (file[i + 1] == NULL && (tmp = reflect_isnull(count_error, type, e)))
		if (tmp == 1)
			return (1);
	if (count_error[0] != 1)
	{
		if (type == LIGHT && type == CAMERA)
		{
			ft_blue("warning: You try to put reflect to the camera or the \
					light.");
			return (1);
		}
		else
		{
			if (is_pos(file[i + 1]) == 1)
				return (count_error[3] = 1);
			e->obj->last->reflect = ft_atof(file[i + 1]);
			return (1);
		}
	}
	return (count_error[3] = 1);
}

int		refract_isnull(int *count_error, int type, t_env *e)
{
	count_error[0] = 1;
	ft_blue("warning: You have forgot one or more value");
	if (type == LIGHT && type == CAMERA)
	{
		ft_blue("warning: You try to put reflect to the light or camera.");
		return (1);
	}
	else
		e->obj->last->refract = 1.0;
	return (0);
}

int		get_refract(t_env *e, char **file, int i, int type)
{
	int		*count_error;
	int		tmp;

	count_error = ft_memalloc(sizeof(int) * 2);
	if (file[i + 1] == NULL && (tmp = refract_isnull(count_error, type, e)))
		if (tmp == 1)
			return (1);
	if (count_error[0] != 1)
	{
		if (type == LIGHT && type == CAMERA)
		{
			ft_blue("warning: You try to put reflect to the camera or the \
					light.");
			return (1);
		}
		else
		{
			if (is_pos(file[i + 1]) == 1)
				return (count_error[3] = 1);
			e->obj->last->refract = ft_atof(file[i + 1]);
			return (1);
		}
	}
	return (count_error[3] = 1);
}
