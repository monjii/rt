/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   primitives.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		impact_paraboloid(t_obj *ptr, t_ray *ray, t_collision *clsn, int fill)
{
	double		res[3];
	double		dist;
	int			impact;

	impact = 0;
	math_paraboloid(res, ray, ptr);
	if ((dist = quadratic(res)) > 0.0)
	{
		if (fill && (clsn->impact_d < 0.0 || dist < clsn->impact_d) &&
			dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
		else if (!fill && dist > 0.0 && dist < clsn->dist_tolight &&
				ptr != (clsn->object))
		{
			if (clsn->shadow_impact < 0.0)
				clsn->shadow_impact = ptr->transparency;
			else
				clsn->shadow_impact *= ptr->transparency;
			impact = 1;
		}
	}
	return (impact);
}

int		impact_sphere(t_obj *ptr, t_ray *ray, t_collision *clsn, int fill)
{
	double		res[3];
	double		dist;
	int			impact;

	impact = 0;
	math_sphere(res, ray, ptr);
	if ((dist = quadratic(res)) > 0.0)
	{
		if (fill && (clsn->impact_d < 0.0 || dist < clsn->impact_d) &&
			dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
		else if (!fill && dist > 0.0 && dist < clsn->dist_tolight &&
				ptr != (clsn->object))
		{
			if (clsn->shadow_impact < 0.0)
				clsn->shadow_impact = ptr->transparency;
			else
				clsn->shadow_impact *= ptr->transparency;
			impact = 1;
		}
	}
	return (impact);
}

int		impact_plane(t_obj *ptr, t_ray *ray, t_collision *clsn, int fill)
{
	double		dist;
	int			impact;

	impact = 0;
	dist = -1.0 * (dot(&ptr->pos, &ray->origin) + ptr->slide) / (ptr->pos.x *
		ray->dir.x + ptr->pos.y * ray->dir.y + ptr->pos.z * ray->dir.z);
	if (fill && (clsn->impact_d < 0.0 || dist < clsn->impact_d) &&
		dist > 0.0)
	{
		fill_collision(ptr, dist, clsn, ray);
		impact = 1;
	}
	else if (!fill && dist > 0.0 && dist < clsn->dist_tolight &&
			ptr != (clsn->object))
	{
		if (clsn->shadow_impact < 0.0)
			clsn->shadow_impact = ptr->transparency;
		else
			clsn->shadow_impact *= ptr->transparency;
		impact = 1;
	}
	return (impact);
}

int		impact_cylinder(t_obj *ptr, t_ray *ray, t_collision *clsn, int fill)
{
	double		res[3];
	double		dist;
	int			impact;

	impact = 0;
	math_cylinder(res, ray, ptr);
	if ((dist = quadratic(res)) > 0.0)
	{
		if (fill && (clsn->impact_d < 0.0 || dist < clsn->impact_d) &&
			dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
		else if (!fill && dist > 0.0 && dist < clsn->dist_tolight &&
				ptr != (clsn->object))
		{
			if (clsn->shadow_impact < 0.0)
				clsn->shadow_impact = ptr->transparency;
			else
				clsn->shadow_impact *= ptr->transparency;
			impact = 1;
		}
	}
	return (impact);
}

int		impact_cone(t_obj *ptr, t_ray *ray, t_collision *clsn, int fill)
{
	double		res[3];
	double		dist;
	int			impact;

	impact = 0;
	math_cone(res, ray, ptr);
	if ((dist = quadratic(res)) > 0.0)
	{
		if (fill && (clsn->impact_d < 0.0 || dist < clsn->impact_d) &&
			dist > 0.0)
		{
			fill_collision(ptr, dist, clsn, ray);
			impact = 1;
		}
		else if (!fill && dist > 0.0 && dist < clsn->dist_tolight &&
				ptr != (clsn->object))
		{
			if (clsn->shadow_impact < 0.0)
				clsn->shadow_impact = ptr->transparency;
			else
				clsn->shadow_impact *= ptr->transparency;
			impact = 1;
		}
	}
	return (impact);
}
