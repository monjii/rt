/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_color.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		parse_color(char *color, int len)
{
	int			i;
	int			true_color;
	char		tmp_color[len];

	i = 0;
	true_color = 0;
	while (color[i])
	{
		if (color[i] >= 'A' && color[i] <= 'F')
			tmp_color[i] = color[i] - 55;
		else if (color[i] >= 'a' && color[i] <= 'f')
			tmp_color[i] = color[i] - 87;
		else
			tmp_color[i] = color[i] - 48;
		i++;
	}
	if (len == 8)
		true_color = hexa_eight(true_color, tmp_color);
	else if (len == 6)
		true_color = hexa_six(true_color, tmp_color);
	return (true_color);
}

void	check_hexa(t_env *e, char **file, int i, int *count_error)
{
	int		j;

	j = 0;
	while (file[i + 1][j])
	{
		if (!(file[i + 1][j] >= 'a' && file[i + 1][j] <= 'f') &&
				!(file[i + 1][j] >= '0' && file[i + 1][j] <= '9'))
		{
			ft_blue("warning: Wrong hexa format. Try to make something like\
				this 0x[0-9 or a-f]");
			if (count_error[1] == LIGHT)
				e->light->last->color = 0xFFFFFF;
			else
				e->obj->last->color = 0xFFFFFF;
			count_error[0] = 1;
			return ;
		}
		j++;
	}
}

void	length_hexa(t_env *e, char **file, int i, int *count_error)
{
	if (file[i + 1] != NULL)
	{
		if (ft_strlen(file[i + 1]) != 6 && ft_strlen(file[i + 1]) != 8)
		{
			ft_blue("warning: Wrong hexa format. Try to make something like\
				this 0x[0-9 or a-f]");
			if (count_error[0] == LIGHT)
				e->light->last->color = 0xFFFFFF;
			else
				e->obj->last->color = 0xFFFFFF;
			count_error[0] = 1;
		}
	}
}

void	choose_color(t_env *e, int type, int i, char **file)
{
	int		len;

	len = ft_strlen(file[i + 1]);
	if (type == LIGHT)
		e->light->last->color = parse_color(file[i + 1], len);
	else
	{
		e->obj->last->color = parse_color(file[i + 1], len);
		if (len == 8)
			e->obj->last->transparency = (double)((e->obj->last->color &
											0xFF000000) >> 24) / 255.0;
	}
}

int		get_color_value(t_env *e, char **file, int i, int type)
{
	int		*count_error;

	count_error = ft_memalloc(sizeof(int) * 2);
	if (type == CAMERA)
	{
		ft_blue("warning: You try to put color to the camera.");
		return (0);
	}
	if (file[i + 1] == NULL && (count_error[0] = 1))
	{
		ft_blue("warning: You have put an invalid hexa format.\
				Try to make something like this 0x[0-9 or a-f]");
		if (type == LIGHT)
			e->light->last->color = 0xFFFFFF;
		else
			e->obj->last->color = 0xFFFFFF;
	}
	if (count_error[0] != 1)
	{
		length_hexa(e, file, i, count_error);
		check_hexa(e, file, i, count_error);
	}
	if (count_error[0] != 1)
		choose_color(e, type, i, file);
	return (1);
}
