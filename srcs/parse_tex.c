/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:53 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:37:55 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		get_texture(t_env *e, char **file, int i, int type)
{
	if (file[i + 1] == NULL)
	{
		ft_blue("warning: You have forget one or more value.");
		e->obj->last->texture = 0;
		return (1);
	}
	else if (type == LIGHT || type == CAMERA)
		return (1);
	else
	{
		if (ft_strcmp(file[i + 1], "perlin") == 0)
			e->obj->last->texture = PERLIN;
		else if (ft_strcmp(file[i + 1], "perturbation") == 0)
			e->obj->last->texture = PERTURBATION;
		else
		{
			ft_blue("Warning: You have forgot one or more value.");
			e->obj->last->texture = 0;
		}
	}
	return (1);
}
