/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_slide.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		slide_isnull(int *count_error, int type, t_env *e)
{
	count_error[0] = 1;
	ft_blue("warning: You have forgot one or more value");
	if (type == LIGHT && type == CAMERA)
	{
		ft_blue("warning: You try to put slide to the light or camera.");
		return (count_error[3] = 1);
	}
	else
		e->obj->last->slide = 1.0;
	return (0);
}

int		which_obj(t_env *e, int type, int i, char **file)
{
	if (type == PARABOLOID || type == PLANE)
	{
		if (is_pos(file[i + 1]) == 1)
			return (1);
		e->obj->last->slide = ft_atof(file[i + 1]);
	}
	else
	{
		ft_blue("warning: You have put slide to obj who don't need");
		return (1);
	}
	return (0);
}

int		get_slide(t_env *e, char **file, int i, int type)
{
	int		*count_error;
	int		tmp;

	count_error = ft_memalloc(sizeof(int) * 4);
	if (file[i + 1] == NULL && (tmp = slide_isnull(count_error, type, e)))
		if (tmp == 1)
			return (1);
	if (count_error[0] != 1)
	{
		if (type == LIGHT && type == CAMERA)
		{
			ft_blue("warning: You try to put slide to the camera or the \
					light.");
			return (1);
		}
		else
			return (which_obj(e, type, i, file));
	}
	return (count_error[3] = 1);
}
