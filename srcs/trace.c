/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		get_color(t_env *e, int color, t_collision *clsn)
{
	int				new_color;
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;
	double			noise;

	r = (color >> 16);
	g = (color >> 8);
	b = color;
	if (clsn->object->texture == PERLIN)
	{
		noise = apply_noise(clsn);
		r = apply_noise_to_color(r, noise);
		g = apply_noise_to_color(g, noise);
		b = apply_noise_to_color(b, noise);
	}
	r *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color >> 16))
			/ 255.0) * (1.0 - e->ambiant_light);
	g *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color >> 8))
			/ 255.0) * (1.0 - e->ambiant_light);
	b *= e->ambiant_light + ((double)((unsigned char)(clsn->light_color)) /
			255.0) * (1.0 - e->ambiant_light);
	new_color = add_spec_to_color(r, g, b, clsn);
	new_color = (e->nb == 1 ? apply_nb(new_color) : new_color);
	return (new_color);
}

t_vec	get_ray_dir(t_env *e, int x, int y)
{
	double	p_w;
	double	p_h;
	t_vec	vec;

	p_w = (double)(SCR_X * e->aa) / 100.0;
	p_h = (double)(SCR_Y * e->aa) / 100.0;
	vec.x = e->ray->origin.x - p_w / 2.0 + (p_w / (double)(SCR_X * e->aa) *
			(double)x);
	vec.y = e->ray->origin.y - p_h / 2.0 + (p_w / (double)(SCR_X * e->aa) *
			(double)y);
	vec.z = p_w / 2.0;
	vec = normalize(&vec);
	//rotate(&vec, e->camera->dir.x, e->camera->dir.y, e->camera->dir.z);
	rotate(&vec, e->camera->rot.x, e->camera->rot.y, e->camera->rot.z);
	return (vec);
}

void	*draw(void *thread_data)
{
	int				x;
	int				y;
	t_env			*e;
	t_collision		*clsn;
	t_thread_data	*th;

	th = (t_thread_data *)thread_data;
	e = th->e;
	x = 0;
	while (x < (SCR_X * e->aa))
	{
		y = th->y_start;
		while (y < th->y_end)
		{
			clsn = init_clsn(e);
			clsn->ray.dir = get_ray_dir(e, x, y);
			if (impact_loop(e, 0, clsn))
				put_pixel_to_img(e, x, y, clsn);
			free_clsn(clsn);
			y++;
		}
		x++;
	}
	free(thread_data);
	return (NULL);
}

void	tracee(t_env *e)
{
	t_thr			thr;
	int				thread_id;
	t_thread_data	*thread_data;

	thread_id = 0;
	pthread_mutex_init(&e->mutex, NULL);
	while (thread_id < THREAD_LIMIT)
	{
		if (!(thread_data = (t_thread_data *)malloc(sizeof(t_thread_data))))
			ft_error("error: Clsn malloc failed in trace.c", 2);
		thread_data->y_start = thread_id * (SCR_Y * e->aa / THREAD_LIMIT);
		thread_data->y_end = (thread_id + 1) * (SCR_Y * e->aa / THREAD_LIMIT);
		thread_data->e = e;
		pthread_create(&thr.thr_id[thread_id], NULL, &draw, thread_data);
		thread_id++;
	}
	thread_id = 0;
	while (thread_id < THREAD_LIMIT)
	{
		pthread_join(thr.thr_id[thread_id], NULL);
		thread_id++;
	}
	return ;
}
