/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_light.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qjacob <qjacob@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 11:22:59 by qjacob            #+#    #+#             */
/*   Updated: 2016/05/04 16:58:04 by qjacob           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rt.h"

int		light_dir(t_env *e, int *count_error, char **file, int i)
{
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->light->last->dir.x = ft_atof(file[i + 1]);
	}
	if (count_error[1] != 1)
	{
		if (is_pos(file[i + 2]) == 1)
			return (count_error[3] = 1);
		e->light->last->dir.y = ft_atof(file[i + 2]);
	}
	if (count_error[2] != 1)
	{
		if (is_pos(file[i + 3]) == 1)
			return (count_error[3] = 1);
		e->light->last->dir.z = ft_atof(file[i + 3]);
	}
	return (0);
}

int		check_light_pos(char **file, int i, int *count_error)
{
	if (file[i + 1] == NULL)
	{
		count_error[0] = 1;
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 1);
	}
	if (file[i + 2] == NULL)
	{
		count_error[1] = 1;
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 2);
	}
	if (file[i + 3] == NULL)
	{
		count_error[2] = 1;
		ft_blue("warning: You have forgot one or more value.");
		return (count_error[3] = 3);
	}
	return (count_error[3]);
}

int		get_light_pos(t_env *e, char **file, int i)
{
	int			*count_error;

	count_error = ft_memalloc(sizeof(int) * 4);
	count_error[3] = check_light_pos(file, i, count_error);
	if (count_error[0] != 1)
	{
		if (is_pos(file[i + 1]) == 1)
			return (count_error[3] = 1);
		e->light->last->pos.x = ft_atof(file[i + 1]);
	}
	if (count_error[1] != 1)
	{
		if (is_pos(file[i + 2]) == 1)
			return (count_error[3] = 2);
		e->light->last->pos.y = ft_atof(file[i + 2]);
	}
	if (count_error[2] != 1)
	{
		if (is_pos(file[i + 3]) == 1)
			return (count_error[3] = 3);
		e->light->last->pos.z = ft_atof(file[i + 3]);
	}
	return (count_error[3]);
}

int		get_type(t_env *e, char **file, int i)
{
	if (file[i + 1] == NULL)
	{
		ft_blue("warning: You have forget one or more value.");
		e->light->last->type = PUNCTUAL;
		return (1);
	}
	else
	{
		if (ft_strcmp(file[i + 1], "punctual") == 0)
			e->light->last->type = PUNCTUAL;
		else if (ft_strcmp(file[i + 1], "directional") == 0)
			e->light->last->type = DIRECTIONAL;
		else if (ft_strcmp(file[i + 1], "parallel") == 0)
			e->light->last->type = PARALLEL;
		else
		{
			ft_blue("Warning: You have forgot one or more value.");
			e->light->last->type = 1;
		}
	}
	return (1);
}
